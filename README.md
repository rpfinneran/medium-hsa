# Medium HSA
This repository is used hand-in-hand with [The Power of Health Savings Accounts (HSA)](https://medium.com/@rpfinneran/the-power-of-health-savings-accounts-hsas-82b1eeac4df1)

The medium article describes WHY you should use an HSA. This repository helps you to understand HOW to track HSA related expenses and be prepared to defend any reimbursement should the IRS audit you.

![HSA Summary](/images/hsa-status-summary.png)  
*Summary report of amounts reimbursed and remaining expenses that CAN be reimbursed*

The HSA-Tracking.xlsx spreadsheet includes formulas to help compute the reimbursable expenses. 

![HSA Tracking Spreadsheet Sample](/images/HSA-tracking.PNG)*HSA Tracking Spreadsheet Accounts for Missing Receipts*

The folder/file structure used is as follows:
```
\HSA
│   HSA-Tracking.xlsx
│
├───Reimbursed QME
│   └───Tax Year 2020
│           20200715-Bluey-Dentist.pdf
│
└───Unreimbursed QME
        20190511-Bingo-Doctor.pdf
        20211031-Dad-EmergencyRoom.pdf
        20221118-Mom-Prescription.pdf

```
All receipts are initially loaded to unreimbursed QME folder. Once they are reimbursed, the receipts move to an appropriate named Tax Year folder in the Reimbursed QME directory. 

Please note, this example includes a scenario where a portion of an expense is unaccounted for (no receipt), which happens occasionally with copays and such wherein the receipt gets lost. The simple idea is that when you reimburse yourself, the IRS could audit you and ask to see proof of the qualified medical expenses. In my approach, that would all be reflected in the appropriate Tax Year folder under reimbursed QME. Nice and easy.

# Be A Smart Consumer
Did you know that medical and prescription prices vary by location, no different the groceries? If you're on an HSA, it is in your best interest to shop for deals, especially with products (versus services). Prescription drugs are an optimal place to save money by shopping for the best price instead of filling at the closest pharmacy to your home or office.

There a many websites that can help you check the prices of prescription drugs at all pharmacies near you in just a few quick clicks. One example is [GoodRX](https://www.goodrx.com/search).

## License
Please see LICENSE.txt


